package ru.phism.intellij.unreal.completion;

import com.intellij.codeInsight.completion.*;
import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.editor.Document;
import com.intellij.patterns.ElementPattern;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.ProcessingContext;
import com.jetbrains.cidr.lang.psi.OCMacroCall;
import org.jetbrains.annotations.NotNull;
import ru.phism.intellij.unreal.icons.UnrealEditorIcons;
import ru.phism.intellij.unreal.lang.FileLangUtil;
import ru.phism.intellij.unreal.engine.property.AllSpecifiers;
import ru.phism.intellij.unreal.engine.property.Specifier;

import static com.intellij.patterns.PlatformPatterns.psiElement;

public class SpecifierCompletionContributor extends CompletionContributor {

    private static final ElementPattern<PsiElement> IN_MACRO_CALL = psiElement().withSuperParent(5, OCMacroCall.class);

    public static final CompletionProvider<CompletionParameters> PARAMETERS_COMPLETION_PROVIDER = new CompletionProvider<CompletionParameters>() {
        @Override
        protected void addCompletions(@NotNull CompletionParameters completionParameters, ProcessingContext processingContext, @NotNull CompletionResultSet completionResultSet) {
            if (FileLangUtil.isUnrealSourceFile(completionParameters.getOriginalFile())) {
                OCMacroCall macroCall = PsiTreeUtil.getParentOfType(completionParameters.getPosition(), OCMacroCall.class);
                if (macroCall != null && macroCall.getMacroReferenceElement() != null) {
                    String macroName = macroCall.getMacroReferenceElement().getText();
                    for (Specifier specifier : AllSpecifiers.getSpecifiersFor(macroName)) {
                        completionResultSet.addElement(createSpecifierLookupElement(specifier));
                    }
                }
            }
        }
    };

    private static InsertHandler<LookupElement> NO_PARAMETERS = new BasicInsertHandler<LookupElement>();
    private static InsertHandler<LookupElement> WITH_PARAMETERS = new BasicInsertHandler<LookupElement>() {
        @Override
        public void handleInsert(InsertionContext insertionContext, LookupElement lookupElement) {
            Document document = insertionContext.getEditor().getDocument();
            int offset = insertionContext.getEditor().getCaretModel().getOffset();
            document.insertString(offset, "=");
            insertionContext.getEditor().getCaretModel().moveToOffset(offset + 1);
        }
    };

    public SpecifierCompletionContributor() {
        extend(CompletionType.BASIC, IN_MACRO_CALL, PARAMETERS_COMPLETION_PROVIDER);
        extend(CompletionType.SMART, IN_MACRO_CALL, PARAMETERS_COMPLETION_PROVIDER);
    }

    private static LookupElement createSpecifierLookupElement(Specifier specifier) {
        return LookupElementBuilder.create(specifier, specifier.getName())
                .withBoldness(true)
                .withIcon(UnrealEditorIcons.SPECIFIER)
                .withInsertHandler(specifier.isSimple() ? NO_PARAMETERS : WITH_PARAMETERS);
    }
}
