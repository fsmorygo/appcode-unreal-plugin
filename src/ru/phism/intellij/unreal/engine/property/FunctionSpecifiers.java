package ru.phism.intellij.unreal.engine.property;

import org.jetbrains.annotations.NotNull;
import ru.phism.intellij.unreal.engine.property.impl.SimpleSpecifier;
import ru.phism.intellij.unreal.engine.property.impl.SingleParameterSpecifier;

/**
 * @note See https://docs.unrealengine.com/latest/INT/Programming/UnrealArchitecture/Reference/Functions/Specifiers/index.html
 */
public enum FunctionSpecifiers implements Specifier {
    BLUEPRINT_AUTHORITY_ONLY(new SimpleSpecifier("BlueprintAuthorityOnly")),
    BLUEPRINT_CALLABLE(new SimpleSpecifier("BlueprintCallable")),
    BLUEPRINT_COSMETIC(new SimpleSpecifier("BlueprintCosmetic")),
    BLUEPRINT_IMPLEMENTABLE_EVENT(new SimpleSpecifier("BlueprintImplementableEvent")),
    BLUEPRINT_NATIVE_EVENT(new SimpleSpecifier("BlueprintNativeEvent")),
    BLUEPRINT_PURE(new SimpleSpecifier("BlueprintPure")),
    CATEGORY(new SingleParameterSpecifier("Category")),
    CLIENT(new SimpleSpecifier("Client")),
    CUSTOM_THUNK(new SimpleSpecifier("CustomThunk")),
    EXEC(new SimpleSpecifier("Exec")),
    NET_MULTICAST(new SimpleSpecifier("NetMulticast")),
    RELIABLE(new SimpleSpecifier("Reliable")),
    SERVER(new SimpleSpecifier("Server")),
    UNRELIABLE(new SimpleSpecifier("Unreliable")),
    ;

    private Specifier mySpecifier;

    FunctionSpecifiers(Specifier specifier) {
        mySpecifier = specifier;
    }

    @NotNull
    @Override
    public String getName() {
        return mySpecifier.getName();
    }

    @Override
    public boolean isSimple() {
        return mySpecifier.isSimple();
    }
}
