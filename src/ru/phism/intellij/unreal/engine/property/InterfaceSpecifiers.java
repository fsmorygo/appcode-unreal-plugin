package ru.phism.intellij.unreal.engine.property;

import org.jetbrains.annotations.NotNull;
import ru.phism.intellij.unreal.engine.property.impl.SimpleSpecifier;
import ru.phism.intellij.unreal.engine.property.impl.SingleParameterSpecifier;

/**
 * @note See https://docs.unrealengine.com/latest/INT/Programming/UnrealArchitecture/Reference/Interfaces/Specifiers/index.html
 */
public enum InterfaceSpecifiers implements Specifier {
    BLUEPRINTABLE(new SimpleSpecifier("Blueprintable")),
    DEPENDS_ON(new SingleParameterSpecifier("DependsOn")),
    MINIMAL_API(new SimpleSpecifier("MinimalAPI")),
    NOT_BLUEPRINTABLE(new SimpleSpecifier("NotBlueprintable")),
    ;

    private Specifier mySpecifier;

    InterfaceSpecifiers(@NotNull Specifier specifier) {
        mySpecifier = specifier;
    }

    @NotNull
    @Override
    public String getName() {
        return mySpecifier.getName();
    }

    @Override
    public boolean isSimple() {
        return mySpecifier.isSimple();
    }
}
