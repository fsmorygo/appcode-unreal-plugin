package ru.phism.intellij.unreal.engine.property;

import com.intellij.util.containers.ContainerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.Set;

public class AllSpecifiers {

    private static final Set<Specifier> PROPERTY_SPECIFIERS = Collections.unmodifiableSet(ContainerUtil.newHashSet((Specifier[])PropertySpecifiers.values()));
    private static final Set<Specifier> FUNCTION_SPECIFIERS = Collections.unmodifiableSet(ContainerUtil.newHashSet((Specifier[]) FunctionSpecifiers.values()));
    private static final Set<Specifier> CLASS_SPECIFIERS = Collections.unmodifiableSet(ContainerUtil.newHashSet((Specifier[]) ClassSpecifiers.values()));
    private static final Set<Specifier> STRUCT_SPECIFIERS = Collections.unmodifiableSet(ContainerUtil.newHashSet((Specifier[]) StructSpecifiers.values()));
    private static final Set<Specifier> INTERFACE_SPECIFIERS = Collections.unmodifiableSet(ContainerUtil.newHashSet((Specifier[]) InterfaceSpecifiers.values()));

    @NotNull
    public static Set<Specifier> getSpecifiersFor(@Nullable String annotation) {
        if ("UPROPERTY".equals(annotation)) {
            return PROPERTY_SPECIFIERS;
        }
        if ("UFUNCTION".equals(annotation)) {
            return FUNCTION_SPECIFIERS;
        }
        if ("UCLASS".equals(annotation)) {
            return CLASS_SPECIFIERS;
        }
        if ("USTRUCT".equals(annotation)) {
            return STRUCT_SPECIFIERS;
        }
        if ("UINTERFACE".equals(annotation)) {
            return INTERFACE_SPECIFIERS;
        }
        return Collections.emptySet();
    }
}


