package ru.phism.intellij.unreal.engine.property;

import org.jetbrains.annotations.NotNull;
import ru.phism.intellij.unreal.engine.property.impl.SimpleSpecifier;
import ru.phism.intellij.unreal.engine.property.impl.SingleParameterSpecifier;

/**
 * @note Read https://docs.unrealengine.com/latest/INT/Programming/UnrealArchitecture/Reference/Properties/Specifiers/index.html
 */
public enum PropertySpecifiers implements Specifier {
    ADVANCED_DISPLAY(new SimpleSpecifier("AdvancedDisplay")),
    ASSET_REGISTRY_SEARCHABLE(new SimpleSpecifier("AssetRegistrySearchable")),
    BLUEPRINT_ASSIGNABLE(new SimpleSpecifier("BlueprintAssignable")),
    BLUEPRINT_CALLABLE(new SimpleSpecifier("BlueprintCallable")),
    BLUEPRINT_READ_ONLY(new SimpleSpecifier("BlueprintReadOnly")),
    BLUEPRINT_READ_WRITE(new SimpleSpecifier("BlueprintReadWrite")),
    CATEGORY(new SingleParameterSpecifier("Category")),
    CONFIG(new SimpleSpecifier("Config")),
    CONST(new SimpleSpecifier("Const")),
    DUPLICATE_TRANSIENT(new SimpleSpecifier("DuplicateTransient")),
    EDIT_ANYWHERE(new SimpleSpecifier("EditAnywhere")),
    EDIT_DEFAULTS_ONLY(new SimpleSpecifier("EditDefaultsOnly")),
    EDIT_FIXED_SIZE(new SimpleSpecifier("EditFixedSize")),
    EDIT_INLINE(new SimpleSpecifier("EditInline")),
    EDIT_INSTANCE_ONLY(new SimpleSpecifier("EditInstanceOnly")),
    EXPORT(new SimpleSpecifier("Export")),
    GLOBAL_CONFIG(new SimpleSpecifier("GlobalConfig")),
    INSTANCED(new SimpleSpecifier("Instanced")),
    INTERP(new SimpleSpecifier("Interp")),
    LOCALIZED(new SimpleSpecifier("Localized")),
    NATIVE(new SimpleSpecifier("Native")),
    NO_CLEAR(new SimpleSpecifier("NoClear")),
    NO_EXPORT(new SimpleSpecifier("NoExport")),
    NON_TRANSACTIONAL(new SimpleSpecifier("NonTransactional")),
    REF(new SimpleSpecifier("Ref")),
    REPLICATED(new SimpleSpecifier("Replicated")),
    REPLICATED_USING(new SingleParameterSpecifier("ReplicatedUsing")),
    REP_RETRY(new SimpleSpecifier("RepRetry")),
    SAVE_GAME(new SimpleSpecifier("SaveGame")),
    SERIALIZE_TEXT(new SimpleSpecifier("SerializeText")),
    SIMPLE_DISPLAY(new SimpleSpecifier("SimpleDisplay")),
    TRANSIENT(new SimpleSpecifier("Transient")),
    VISIBLE_ANYWHERE(new SimpleSpecifier("VisibleAnywhere")),
    VISIBLE_DEFAULTS_ONLY(new SimpleSpecifier("VisibleDefaultsOnly")),
    VISIBLE_INSTANCE_ONLY(new SimpleSpecifier("VisibleInstanceOnly")),
    ;

    private Specifier mySpecifier;

    PropertySpecifiers(@NotNull Specifier specifier) {
        mySpecifier = specifier;
    }

    @NotNull
    @Override
    public String getName() {
        return mySpecifier.getName();
    }

    @Override
    public boolean isSimple() {
        return mySpecifier.isSimple();
    }
}
