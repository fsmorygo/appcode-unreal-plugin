package ru.phism.intellij.unreal.engine.property;

import org.jetbrains.annotations.NotNull;

public interface Specifier {

    @NotNull
    String getName();

    boolean isSimple();
}
