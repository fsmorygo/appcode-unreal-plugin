package ru.phism.intellij.unreal.engine.property.impl;

import org.jetbrains.annotations.NotNull;
import ru.phism.intellij.unreal.engine.property.Specifier;

public class SimpleSpecifier implements Specifier {
    private final String myName;

    public SimpleSpecifier(String myName) {
        this.myName = myName;
    }

    @Override
    @NotNull
    public String getName() {
        return myName;
    }

    @Override
    public boolean isSimple() {
        return true;
    }
}
