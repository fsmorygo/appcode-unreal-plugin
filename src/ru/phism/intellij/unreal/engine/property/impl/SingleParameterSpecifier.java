package ru.phism.intellij.unreal.engine.property.impl;

import org.jetbrains.annotations.NotNull;
import ru.phism.intellij.unreal.engine.property.Specifier;

public class SingleParameterSpecifier implements Specifier {

    private final String myName;

    public SingleParameterSpecifier(String myName) {
        this.myName = myName;
    }

    @Override
    @NotNull
    public String getName() {
        return myName;
    }

    @Override
    public boolean isSimple() {
        return false;
    }
}
