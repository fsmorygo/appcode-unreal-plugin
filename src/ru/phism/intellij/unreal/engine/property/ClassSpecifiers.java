package ru.phism.intellij.unreal.engine.property;

import org.jetbrains.annotations.NotNull;
import ru.phism.intellij.unreal.engine.property.impl.SimpleSpecifier;
import ru.phism.intellij.unreal.engine.property.impl.SingleParameterSpecifier;

/**
 * @note See https://docs.unrealengine.com/latest/INT/Programming/UnrealArchitecture/Reference/Classes/Specifiers/index.html
 */
public enum ClassSpecifiers implements Specifier {
    ABSTRACT(new SimpleSpecifier("Abstract")),
    ADVANCED_CLASS_DISPLAY(new SimpleSpecifier("AdvancedClassDisplay")),
    AUTO_COLLAPSE_CATEGORIES(new SingleParameterSpecifier("AutoCollapseCategories")),
    AUTO_EXPAND_CATEGORIES(new SingleParameterSpecifier("AutoExpandCategories")),
    BLUEPRINTABLE(new SimpleSpecifier("Blueprintable")),
    BLUEPRINT_TYPE(new SimpleSpecifier("BlueprintType")),
    CLASS_GROUP(new SingleParameterSpecifier("ClassGroup")),
    COLLAPSE_CATEGORIES(new SimpleSpecifier("CollapseCategories")),
    CONFIG(new SingleParameterSpecifier("Config")),
    CONST(new SimpleSpecifier("Const")),
    CONVERSION_ROOT(new SimpleSpecifier("ConversionRoot")),
    CUSTOM_CONSTRUCTOR(new SimpleSpecifier("CustomConstructor")),
    DEFAULT_TO_INSTANCED(new SimpleSpecifier("DefaultToInstanced")),
    DEPENDS_ON(new SingleParameterSpecifier("DependsOn")),
    DEPRECATED(new SimpleSpecifier("Deprecated")),
    DONT_AUTO_COLLAPSE_CATEGORIES(new SingleParameterSpecifier("DontAutoCollapseCategories")),
    DONT_COLLAPSE_CATEGORIES(new SimpleSpecifier("DontCollapseCategories")),
    EDIT_INLINE_NEW(new SimpleSpecifier("EditInlineNew")),
    HIDE_CATEGORIES(new SingleParameterSpecifier("HideCategories")),
    HIDE_DROPDOWN(new SimpleSpecifier("HideDropdown")),
    HIDE_FUNCTIONS(new SingleParameterSpecifier("HideFunctions")),
    INTRINSIC(new SimpleSpecifier("Intrinsic")),
    MINIMAL_API(new SimpleSpecifier("MinimalAPI")),
    NO_EXPORT(new SimpleSpecifier("NoExport")),
    NON_TRANSIENT(new SimpleSpecifier("NonTransient")),
    NOT_BLUEPRINTABLE(new SimpleSpecifier("NotBlueprintable")),
    NOT_PLACEABLE(new SimpleSpecifier("NotPlaceable")),
    PER_OBJECT_CONFIG(new SimpleSpecifier("PerObjectConfig")),
    PLACEABLE(new SimpleSpecifier("Placeable")),
    SHOW_CATEGORIES(new SingleParameterSpecifier("ShowCategories")),
    SHOW_FUNCTIONS(new SingleParameterSpecifier("ShowFunctions")),
    TRANSIENT(new SimpleSpecifier("Transient")),
    WITHIN(new SingleParameterSpecifier("Within")),
    ;

    private Specifier mySpecifier;

    ClassSpecifiers(@NotNull Specifier specifier) {
        mySpecifier = specifier;
    }

    @NotNull
    @Override
    public String getName() {
        return mySpecifier.getName();
    }

    @Override
    public boolean isSimple() {
        return mySpecifier.isSimple();
    }
}
