package ru.phism.intellij.unreal.icons;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class UnrealEditorIcons {
    private static Icon load(String path) {
        return IconLoader.getIcon(path, UnrealEditorIcons.class);
    }

    public static final Icon SPECIFIER = load("/icons/unreal.png");
}
